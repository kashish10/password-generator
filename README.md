**Password Generator**

A simple website to create a random password with several options like whether to include uppercase characters, numbers or special characters. Learnt how to:

- Install Django
- Create a new project
- Work with Django's URL routing
- Creating Templates
- Submit and handle HTML form data

Technologies used : 
- Django
- Bootstrap
- Python
- Html/CSS

**What is Django?**

Django is an open-source web framework that is written in Python. It was created about twelve years ago, but new in 2020 we have Django 3 with a whole new set of features and capabilities!

Django is an amazing framework for web developers because it provides the infrastructure required for database-driven websites that have user authentication, content administration, contact forms, file uploads, and more. Instead of creating all of these features from scratch, you can use the Django framework and utilize these components that are already built, and focus your time on developing your web app instead.

If you're going to be working with Python, especially for web applications or web design, you'll want to learn the Django framework. It will save you a ton of time!

**Usage**

- Clone the project
- Install django and python
- Go inside the password generator directory
- Run : _python manage.py runserver_
- go to : http://127.0.0.1:8000/
